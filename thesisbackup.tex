\documentclass[12pt]{report}
%%%%font package fghfgnfgnfgnfgn
\usepackage[utf8]{inputenc}
%\usepackage{tgbonum}
%%%% font package end
\usepackage{fullpage}
\usepackage{tikz,graphics,color,fullpage,float,epsf,caption,subcaption}
\usepackage{cite}
\usepackage{natbib}
\usepackage{wrapfig}
\usepackage{natbib}  
\bibliographystyle{plainnat}
\usepackage[nottoc,notlot,notlof]{tocbibind}
\usepackage{parskip}
\usepackage{fullpage}
\usepackage{ragged2e}
\usepackage[font={scriptsize}]{subcaption}
\usepackage[font=footnotesize,labelfont=bf]{caption}
\usepackage{graphicx}
\usepackage{siunitx}
\usepackage[final]{pdfpages}
\graphicspath{ {figures/} }
%\usepackage[pass]{geometry}

\usepackage{url}
\usepackage{indentfirst}
\renewcommand{\baselinestretch}{1.5} 
\usepackage{hyperref}
\hypersetup{
   colorlinks,
   citecolor=violet,
   filecolor=black,
   linkcolor=violet,
   urlcolor=violet
}
\usepackage{afterpage}


\newcommand\blankpage{%
    \null
    \thispagestyle{empty}%
    \addtocounter{page}{+1}%
    \newpage}
    
   
\graphicspath{ {./images/} }
\begin{document}
\pagenumbering{gobble}
\afterpage{\blankpage}
\begin{titlepage}
\begin{center}
  {\LARGE Surface Tension of Flowing Soap Films\par}
 \vspace{2cm}
 \includegraphics[scale=0.3]{brownlogo.png}\par
 \vspace{2cm}
 {Aakash Sane\\School of Engineering\\Brown University}\par
 \vspace{3cm}
 {Submitted in partial fulfillment of the requirements \\for
the Degree of Master of Science
in the \\School of Engineering at Brown University}\par

\end{center}
\end{titlepage}

%%%%%%%%%%%%%%%%%
%\pagenumbering{gobble}
\afterpage{\blankpage}

\includepdf[pages={1}]{signature.pdf}
%%%%%%%%%%%%%%%%%


\includepdf{authorization.pdf}

\justify

\begin{abstract}\
We investigate the relation of surface tension of the flowing soap films with respect to flow parameters such as the flow speed, width of the film and concentration of the soap solution. We use a relation between surface tension and curvature of the bounding wires to measure surface tension. Our measurements indicate that the surface tension is 0.027 N/m for soap films made using commonly used soap (Dawn dishwashing soap). For dilute solutions, the surface tension value increases as film becomes thinner. This increase may be understood by noting that thinning of the film is equivalent to dilution of the solution. A thinner film has greater surface area to volume ratio and adsorpion of soap molecules to the surface decreases the bulk concentration thereby diluting the solution. Our results not only shed light on a previous unknown feature of constant surface tension of flowing soap films but also support the claim that the elasticity of flowing soap films does not vary significantly. 
\end{abstract}

\newpage
\blankpage
\section*{Acknowledgements}
% \addcontentsline{toc}{section}{Acknowledgements}
I would like to thank Dr. Shreyas Mandre for accepting me as his student and providing his valuable time advising me on this project. I would also like to thank Dr. Ildoo Kim for guiding me during experiments and helping me to understand soap films. \\
I would also like to thank Prerna Patil for assisting me in experiments. Also, a fresh pair of eyes provided by Dr. Harsh Soni were valuable in making this thesis a bit more comprehensible.


\newpage
\blankpage
\section*{Dedication}
Dedicated to my parents, without their love and care I would have achieved nothing. 

\newpage
\blankpage
\tableofcontents
%\blankpage
\listoffigures
\newpage
\justify
\setlength{\parindent}{2em}

\chapter{Introduction}
\pagenumbering{arabic}
\section{Introduction}

Soap films are considered as two-dimensional fluids and have been used by the fluid dynamics community to understand two dimensional phenomenon such as 2D turbulence, flapping of flags, etc. Although the soap films have been assumed to be two-dimensional, they are known to exhibit thickness changes which make the films three-dimensional. The three dimensional effects can be characterized by the elasticity or the surface tension of the soap film. In a recent work by \cite{kim2016marangoni} elasticity of the soap films has been found to be constant and they speculated that the surface of the soap film is saturated with soap molecules. The aim of the present work is two fold - to measure and understand surface tension of flowing soap films with respect to flow parameters and to investigate the claim of saturated soap film surface made by \cite{kim2016marangoni}.\\

Measuring surface tension of a soap film is particularly difficult because existing measurement techniques intrude the soap film or use a sample of solution which involves changing the geometry. These measurement techniques are unfavorable as they will change the surface tension due to the Marangoni effect. To overcome this difficulty, we have  developed a non-intrusive technique, for which we have derived a relation between the surface tension and the deflection of nylon wires which hold the flowing soap films.\\

Chapter 1 starts with introducing soap films and explains the Marangoni effect which causes stability and 2D compressibility. The uses of soap films and the work of \cite{kim2016marangoni} have been stated in chapter 1. The difficulty in measuring surface tension using traditional techniques has been identified in chapter 1. Chapter 2 explains our novel and non-intrusive technique to measure surface tension. The results of our experiments are presented in Chapter 3 which includes the discussion of our results.\\

\section{Soap Films}
Soap films  are observed everyday in the form of soap bubbles, foams, etc. and are not only visually appealing but also fun to play with. Soap films are thin sheets of liquid surrounded by air on both sides of its interface. The interface consists of layers of soap molecules and the interstitial fluid is sandwiched between these two layers of surfactants. These layers of surfactants makes the formation of soap bubbles possible as opposed to making bubbles from pure water. 
\begin{figure}[h!]
   \centering
   \begin{subfigure}[t]{0.4\textwidth}
       \centering
       \includegraphics[height=2.2in]{soapbubble.jpg}
       \caption{Soap bubble \cite{bubbleimage}}
   \end{subfigure}
   \begin{subfigure}[t]{0.4\textwidth}
       \centering
       \includegraphics[height=2.2in]{soapfilmframe.jpg}
       \caption{Soap film on a frame \cite{soapfilmframe} }
   \end{subfigure}
   \caption{Soap films}
   \label{fig:Soap films}
\end{figure}
An attempt to make bubbles out of pure water is futile because they will burst instantaneously. Thin liquid sheets made using pure water are unstable and break up as documented by Savart (\cite{sheetbreakup}) while observing the edge of a moving liquid sheet with air on both sides. A natural question arises: ``Is it possible to make a liquid sheet which is stable?".
\begin{figure}[h!]
\center
\includegraphics[scale=0.4]{crosssect}
\caption{Cross section of a typical soap film. The hatched area has soap solution and the interfaces have soap molecules acting as surfactants. The wires, shown as gray shaded circles, hold the film. The slab of film attaches itself to the wires and this region is called as plateau border (\cite{xlwu})}
\label{fig:cross section of soap film}
\end{figure}
Soap is required to make stable thin sheets of liquid with air at both interfaces. Soap molecules added to pure water act as surfactants and change the dynamics of liquid sheets. The surfactants give rise to the Marangoni effect which is responsible in providing stability to the soap film. The Marangoni effect depends on the surface tension of the interface and will be explained in the following sections. 
\section{Surface Tension}
\label{sec:surfacetension}
Surface tension is a property of the interface between two fluids and is an interfacial force between two mediums. The book on capillarity by \cite{pierregennes} describes the origin of surface tension as the deficiency in the energy faced by a molecule which attaches itself to the interface.
\begin{figure}[h!]
\center
\includegraphics[scale=0.3]{surfmolec.jpg}
\caption{The molecule at the surface has less energy than the one in the bulk (\cite{pierregennes})}
\label{Surface Molecule}
\end{figure}
There are two equivalent definitions of surface tension - in terms of forces and in terms of energy. In terms of force, surface tension can be defined as the tangential force (tangent to the surface) experienced by a segment of unit length on the surface. The force has to be tangent to the surface and perpendicular to the segment (\cite{pierregennes}). In terms of energy, surface tension is the energy that needs to be supplied to increase the interfacial area by one unit. The SI unit of surface tension is N/m.\\

Surface tension depends on the concentration of surfactants on the surface. Surfactants are surface acting agents and consist of a hydrophobic part which is insoluble in water and a hydrophilic part which is soluble. A surfactant, because of its hydrophobic part, adsorbs onto the surface and rearranges so as to have hydrophobic part outside water.  As soon as the surface is saturated by surfactant molecules, they start forming spherical groups called as micelles inside the solution such that the hydrophobic ends remain pointing inside and hydrophilic ends point outwards of the group (\cite{surfactantbook}). \\

\section{Marangoni Effect in Soap Films}
\label{sec:marangoni}
If the surface tension is not uniform, it causes a stress on the fluid which tends to push it into region of high surface tension. The stress is called as Marangoni stress and the effect can be classified as the Marangoni effect. In soap films, the spatial variation of surface tension occurs because of the difference in the surface concentration of the soap molecules. 

Disturbance occuring in the soap film on the short time scale can cause thinning and may tend to break the soap film. Due to thinning a local squeezing of the film occurs (shown in gray shaded region in Fig. \ref{fig: Marangoni effect in soap film}). Squeezing the film will push out the fluid into the surrounding region of the soap film. As the bulk fluid spreads due to squeezing, it occupies a larger interfacial area in accordance with the conservation of mass. The sudden increase in area gives rise to instantaneous decrease in surface concentration of soap molecules. The abrupt decline in surface concentration leads to increase in surface tension in the gray shaded region and can be understood by looking at the equation of state $ \sigma = \sigma_{pw} - RT\Gamma $, where $\sigma$ stands for surface tension, $pw$ for pure water, $R$ = 8.314 J/mol.K is the universal gas constant, $T$ is the temperature of the liquid and $\Gamma$ is the surface concentration of the surfactants. The increased surface tension pulls back the fluid into the thinned (gray shaded) region and restores the film's thickness.\\

\begin{figure}[h!]
\centering
\includegraphics[scale=0.5]{marangonieffect}
\caption{Cross sectional slab of a soap film. Thinning gives rise to spatial variation of surface tension. The increased surface tension in the gray shaded region pulls fluid back into it and restores the film to its original thickness. This effect is called as the Marangoni effect.}
\label{fig: Marangoni effect in soap film}
\end{figure}

\section{Elasticity of Soap Films}
The stability mechanism, which arises because of the Marangoni effect, also provides the soap film with elasticity. 
In soap films, elasticity has been defined as the ratio of change in surface tension to the relative change in thickness and is given by
\begin{equation}
E = 2  \left( \frac{d\sigma}{dh/h}\right),
\label{eq:elasticity}
\end{equation}
Where $E$ is the elasticity, $\sigma$ is the surface tension and $h$ is the thickness \citep{couder1989}. The unit of elasticity is force per unit length (N/m). In-spite of elasticity which stabilizes the film and enforces two-dimensionality, thickness changes are observed which makes the film three-dimensional. To match the boundary conditions, soap film flows are known to show hydraulic jumps which manifest in the form of thickness changes as observed by \cite{xlwu}, \cite{obliqueshock}, \citep{Tran2009}, and \cite{kim2016marangoni}.   
Local thinning or thickening of the film introduces restoring forces in the form of spatial variation of the surface tension which brings back the film to its original shape. This elastic nature makes the film stable and prevents it from breaking down into sheets or jets. The elastic nature and stable nature of the soap films have made it possible to utilize soap films to perform two-dimensional fluid experiments.
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Uses of Soap Films and their Three Dimensional Nature}
\label{sec:usesofsoapfilm}
\cite{couder2dturbulence} was the first one to show experimentally that a soap film can be used to observe two-dimensional turbulence, which was previously studied only theoretically and numerically. One exercise of soap film has been to understand dynamics of flexible bodies in motion. \cite{flappingflag} has delineated the dynamical states with which a flexible filament oscillates in a flowing soap film. They have reported the bi-stability of a flexible filament which can be used to understand swimming of fish as well as motion of flapping flags in 3D wind. These experiments were possible because of the stable and two-dimensional nature of soap films.\\

Although the soap film has been assumed to be two-dimensional, the three-dimensional effects exhibited by soap films cannot be neglected. If the speed of the flowing film exceeds the Marangoni wave speed, then the flow can be said to be in the compressible \footnote{When we are talking about compressibility of soap films, we mean two dimensional compressibility and it should not be confused with three dimensional compressibility exhibited by gases. In compressible gases there is a change in density and in soap films there is analogous change in thickness. The change in thickness and its restoration due to the Marangoni effect can be thought of as similar to change in density in gases and increase in pressure resisting the compression of the gas.} regime where thickness effects are non-negligible and out of the plane motion becomes visible in the form of hydraulic jumps. \cite{Rutgers1996} states about the compressible nature of soap films and has been careful enough to perform experiments in the subsonic regime. The observance of normal shocks, similar to normal shocks in three-dimensional gas dynamics, has been reported by \cite{Tran2009} whereas oblique shocks have been reported by \cite{obliqueshock} and \cite{kim2016marangoni}.\\

The definition of elasticity aids in quantifying the compressiblity of soap film. Compressibility can be measured by the Mach number of the flow which is given by the ratio of flow speed to wave speed, $Ma = u/V_m$. The wave speed is the speed with which Marangoni waves are traveling in the film and the wave speed depends on elasticity. The relation has been derived in detail in Appendix \ref{sec:appendixelasticitywavespeed} and is given by:
\begin{equation}
V_m = \sqrt{\frac{E}{\rho h}},
\label{eq:elastiwave1}
\end{equation}
where $Ma$ is the mach number, $u$ is the flow speed and $V_m$ is the marangoni wave speed. Analogous to oblique shock waves which are observed in 3D compressible gases, there are oblique hydraulic jumps observed in soap films due to obstructions such as a needle pierced into the flow field. Oblique hydraulic jumps have been observed and reported by \cite{xlwu}, \cite{obliqueshock}, and \cite{kim2016marangoni}. In 3D compressible gases there is a relation between the wedge angle which produces oblique shockwaves and the angle ($\beta$) made by the shock wave with the incoming flow. A similar relation can be derived for oblique hydraulic jumps in soap films, and for zero wedge angle, the relation given in \cite{kim2016marangoni} is:
\begin{equation}
sin(\beta) = \frac{1}{Ma} = \frac{V_m}{u}
\label{eq:betamach}
\end{equation}
As can be seen by using eq. \ref{eq:elastiwave1} and eq. \ref{eq:betamach}, measuring the flow speed and the angle made by the oblique hydraulic jump with the incoming flow speed for a particular thickness $h$, elasticity can be found out. The value of elasticity as stated by \cite{kim2016marangoni} is 22 $\pm$ 1 dyn/cm and is found out to be constant for a wide range of parameters like the flow rate, width, and concentration of the soap films. \cite{kim2016marangoni} have speculated that as the elasticity is constant, it might be due to the fact that the soap film interface is saturated with soap molecules or the surface tension and surface concentration behave in such a way as to make the elasticity constant. This hypothesis can be verified by measuring the surface tension for different parameters. If the surface tension comes out to be constant, then by the equation of state ($\sigma=\sigma_{pw} - RT\Gamma$) the surface concentration will come out to be constant implying the surface is saturated with soap molecules.\\

The existing techniques for measuring surface tension of a solution involve changing the geometry (for e.g. pendant drop method) or introducing a deformable object into the flow (\citep{Adami2015}). Changing the geometry will not yield the correct result for soap films because soap molecules will re-distribute on the new geometry's surface which will change the surface tension. Introducing a deformable object will cause hydraulic jump (oblique shock) thus changing the surface tension. Hence, the need to measure the surface tension in-situ and without intrusion has led us to come up with a novel technique which we communicate in the next section.

\newpage
\chapter{Measuring Surface tension}

\section{Experimental setup}
\label{sec:experimentalsetup}
A flowing soap film apparatus consists of a soap film which has been drawn between two wires as shown in the figure below.
\begin{figure}[h!]
\center
\includegraphics[scale=0.7]{sfsketch.png}
\caption{Sketch of soap film setup. Overhead tank is filled with soap solution of certain concentration. The solution flows through a valve and flows between the nylon wires creating a soap film. The nylon wires are pulled apart from corners marked as A, B, C, and D. A suspended weight at the bottom maintains the nylon wires vertical and imparts tension in the nylon wires. The drained solution is collected at the bottom in a container (no shown in figure).}
\label{fig:Soap film sketch}
\end{figure}
The setup consists of an overhead tank, a valve, nylon wires, suspended weight, and a tank beneath (tank not shown in Fig.\ref{fig:Soap film sketch} on page \pageref{fig:Soap film sketch}) to collect drained soap solution. \\

To create a flowing soap film the following steps are followed:
Initially the two nylon wires are held touching each other. The soap solution is allowed to drip from the overhead tank along the two nylon wires. The nylon wires are pulled apart using the four supporting wires (shown as A, B, C, and D in above figure) and the soap film is created. The suspended weight at the bottom keeps the wires vertical. \\

Once created, the soap film is stable and soap solution is replenished from the top which prevents the film from draining out.  The flow is contained within the two surfactant layers and typical thickness of the film is around 10 $\mu$m. The small and uniform thickness compared to the width and length makes the flow two-dimensional. 
\section{Method to Measure the Surface Tension}

\begin{figure}[h!]
\center
 \includegraphics[scale=0.14, angle=90]{c02w06f3cropped.JPG}
 \caption{The central two white wires which are bent inwards are the bounding wires. }
 \label{fig: wirecurveinward}
\end{figure}

The forces and moments on an infinitesimal element of wire are: (i) Surface tension, (ii) Tension in the wire due to suspended weight, (iii) Gravity, (iv) Bending resistance and (v) Viscous drag due to the flowing soap solution. Out of these only surface tension and tension constitute the dominant force balance. The magnitude of all the forces has been estimated and the detailed force balance derivation has been given in Appendix \ref{sec:appendixsurfacetensioncurvature}. The nylon wires, between which the soap film flows, deflect and curve inwards because of the surface tension. The deflection causes curvature and our method to measure surface tension involves measuring the curvature of the nylon wires.  \\

The equation which relates the surface tension $\sigma$ of the wire to its curvature $\kappa = \partial^2y/\partial x^2$ and the tension in the string has been derived in Appendix \ref{sec:appendixsurfacetensioncurvature} and is given as:  
\begin{equation}
2\sigma = T \kappa,
\label{eq:sigkappa}
\end{equation}
where $T$ is the tension in the wire in Newtons.

%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}[h!]
 
   \begin{figure}[h!]%{1\textwidth}
   %\captionsetup{width=0.8\textwidth}
       \centering
       \includegraphics[height=15cm,keepaspectratio]{film50points.png}
       \caption{50 points picked manually, shown in cyan. These points act as a guide to the code to recognize the wire.}
       \label{fig:Wire recognized manually}
   \end{figure}%
  % \hfill
   \begin{figure}[h!]%{1\textwidth}
   %\captionsetup{width=0.8\textwidth}
       \centering
       \includegraphics[height=15cm,keepaspectratio]{film3000points}
       \caption{3000 points recognized by code based on 50 points picked manually, shown in red. The code identifies these points based on maximum intensity. }
       \label{fig:Wire recognized by computer code}
   \end{figure}%
   %\hfill
   \begin{figure}[h!]%{1\textwidth}
   %\captionsetup{width=0.8\textwidth}
       \centering
       \includegraphics[height=15cm,keepaspectratio]{film3000pointszoom}
       \caption{Zoomed image of the points recognized by code.}
       \label{fig: zoomed wire}
   \end{figure}
    %\caption{Wire recognized by combining manual recognition and computer code}
   %\label{fig:wire recognition}
%\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%
The method used to find out the shape of the wire involves taking an image and picking points using Matlab's 'ginput' function. The manually picked points (Fig. \ref{fig:Wire recognized manually}) are used as a guide for a code to recognize all the points on the wire. Around 3000 wire pixels having highest value of intensity are detected by the code. Fig. \ref{fig:Wire recognized by computer code} shows 3000 points and Fig. \ref{fig: zoomed wire} shows a zoomed section of Fig. \ref{fig:Wire recognized by computer code} . A quadratic polynomial is fitted onto a set of N consecutive points and its second derivative is found out to get the curvature for those N points. This procedure is repeated for every N consecutive points on the wire. Curvature is found out along the length of the wire and using Eq. \ref{eq:sigkappa}, surface tension is measured. As the variation of surface tension along the length of the soap film seems to not follow a consistent pattern for all the cases, the surface tension is averaged over the length of the film. \\


The experiment was repeated for five different concentrations: 0.5\%, 1\%, 2\%, 4\% and 6\%. Four flow rates were used- 0.26, 0.6, 1.16, 1.55 ml/s and the width of the soap film (width is the length of soap film along y-direction as shown in Fig. \ref{fig:Soap film sketch}) were fixed at 6cm, 8cm, 10cm, 12cm, and 14cm. Dawn dishwashing soap, a commonly available soap was added to de-ionized water to create the soap solution.  \\
\hrulefill
\newpage
\chapter{Results and Discussion}

\section{Results}
The results of the experiments are given in the Fig. \ref{fig:finalresult} below:
\begin{figure}[h!]
\center
 \includegraphics[scale=0.9]{finalresult}
 \caption{Surface tension w.r.t to the flow rate per unit width for different concentrations of soap solution. $\sigma$ stands for surface tension, $q$ is the flow rate of the soap solution, and $w$ is the width of the soap film. The quantity $q/w$ has been used on the x-axis because flow rate per unit width is a direct indication of thickness of the soap film. Thickness increases with increase in $q/w$.}
\label{fig:finalresult}
\end{figure}
The data points have been plotted according to the concentration of the soap solution used.  The flow rate per unit width is an indication of the thickness of the film \cite{Wu2001}. The surface tension value at each data point represents the average surface tension of the entire soap film for a given flow rate per unit width and concentration of soap solution. It is clear from the results that the surface tension for high flow rate per unit width for any particular concentration approaches a constant value of about 0.027 N/m. 

\section{Error analysis}
The error in recognizing the points on the wire leads to error in evaluating the surface tension. Let $f(x)$ be the qudratic polynomial fitted onto the set of N points, and let $g(x)$ represent the N points. The error arises because $g(x)$ has been found out by finding maximum of intensities which differs from $f(x)$.

Fig. \ref{fig: zoomedwire} shows a portion of wire with $f(x)$ in blue color and $g(x)$ in red and the image clearly shows that there is a difference between $f(x)$ and $g(x)$ which is the source of error.
\begin{figure}[h!]
\center
 \includegraphics[scale=1.2]{zoomedwireimage.png}
 \caption{Portion of wire with $f(x)$ (blue color) and $g(x)$ (red color) superimposed. $g(x)$ represents the wire recognized by computer code. $f(x)$ is the quadratic polynomial fitted onto $g(x)$. The difference between $g(x)$ and  $f(x)$ is a source of error.}
 \label{fig: zoomedwire}
\end{figure}
The root-mean-square error is given by:
\begin{equation}
E_{rms} = \sqrt{\frac{\sum{(f(x)-g(x))^2}}{N}}
\end{equation}
$E_{rms}$ is the error in y-position of points for N points. The error in $x$, lets say $\Delta x$ is the length of those N points. The error in surface tension $E_{surf}$ has been evaluated by:
\begin{equation}
E_{surf}= \frac{E_{rms}}{{\Delta x}^2} \frac{W}{4}
\end{equation}

For N=1000, Fig. \ref{fig:surfacetensionwitherror} shows the surface tension along the length evaluated for soap film of 4\% concentration, width of 6 cm and having a flow rate of 1.55 ml/s. The graph shows that the surface tension does not seem to follow any pattern along the length of the wire nor does the error in surface tension follows any fixed pattern for all the cases. The graph for all the cases is similar to the one shown in Fig. \ref{fig:surfacetensionwitherror}. Fig. \ref{fig:finalresult} also indicates the presence of measurement error in the  experiments.
\begin{figure}[h!]
\centering
\includegraphics[scale=0.8]{surfacetension.png}
\caption{Surface tension $\sigma$ with error in surface tension $E_{surf}$ plotted along the length of the soap film. Surface tension seems not to follow any fixed pattern along the length of the soap film.  }
\label{fig:surfacetensionwitherror}
\end{figure}
The surface tension reported in Fig. \ref{fig:finalresult} is the average of $\sigma$ over the entire length of the soap film and the error is the average of $E_{surf}$. For the case shown in Fig. \ref{fig:surfacetensionwitherror} above, the surface tension value is 2.7 $\times$ 10$^{-2}$ N/m with an error $E_{surf} = \pm 2.9 \times 10^{-4} $ N/m.
\newpage


\section{Discussion}
Fig. \ref{fig:finalresult} indicate that the surface tension approaches a constant value at high flow rate per unit width for all the concentrations. High flow rate per unit width implies thicker soap films (\cite{Wu2001}). Considering the equation of state for a soap film, (\cite{couder1989}) $\sigma = \sigma_{pw} - RT\Gamma$, 
where the subscript $pw$ stands for pure water, $R$ is the universal gas constant (R= 8.314 J/mol.K), $T$ is the temperature of the film, we can conclude that if the surface tension approaches a constant value, then the surface concentration $\Gamma$ should also approach a constant value. In accordance with Fig. \ref{fig:finalresult} we propose a relation to capture the variation of surface concentration w.r.t to the bulk concentration, 
\begin{equation}
\Gamma = \Gamma_{\infty} \left( 1 - \frac{{{\eta}^2}C_{cmc}}{C_b} + ...   \right),
\label{eq:gammacmc}
\end{equation}
\begin{equation}
\Gamma_{(C_{b} = C_{cmc})}= \Gamma_{{\infty}} (1-{{\eta}^2}),
\end{equation}
where the term ${\eta}^2$ is unknown.
\begin{figure}[h!]
\centering
\includegraphics[scale=1]{surfbulkconc.png}
\caption{Surface Concentration of soap molecules absorbed at the surface vs bulk concentration of the soap film \citep{couder1989}. Curve is for SDS, but it can be qualitatively used for any kind of surfactant solution. The curve shows the surface concentration approaching a constant value for high bulk concentration values (shown as B-C). Our results show that we are in the B-C region of this curve and our proposed relation (eq.  \ref{eq:gammacmc}) captures this regime.}
\label{fig:surfbulkconc} 
\end{figure}

\section{Conclusion}
Surface tension for all concentrations approaches a value of 0.027 N/m for thick films which implies surface concentration attains a constant value. As surface concentration approaches a constant value it can be inferred that the surface gets saturated with soap molecules for all concentrations at high flow rate per unit width of the soap films. The film is thick enough for the bulk to provide the surface with soap molecules in order to saturate it. Our results support the claim of \cite{kim2016marangoni} that the surface is saturated with soap molecules. It can also be speculated that the bulk interstitial fluid has the possibility of micelles being present.\\

It can inferred from Fig. \ref{fig:finalresult} that thinning the film has equivalent effect as to diluting the film. This can be attributed to the fact that a thinner film has higher surface area to volume ratio which leads to higher adsorption of soap molecules to the surface. The molecules to the surface come from the bulk thereby diluting the solution. This effect is in accordance to the dilution equation $C = C_b + (2\Gamma/h)$, equation for conservation of surfactant molecules. It can also be observed from Fig. \ref{fig:finalresult} that the dilution effect is predominant for lower concentrations of the soap solutions. However, for higher concentrations ($>$ 2\%) bulk of the soap film always has enough molecules to provide to the surface and hence the thinning does not lead to significant dilution as compared to lower concentrations. 




\newpage
\appendix

\chapter{}

\section{Relation between surface tension and curvature}
\label{sec:appendixsurfacetensioncurvature}
\begin{figure}[h!]
   \centering
   \begin{subfigure}[t]{0.5\textwidth}
       \centering
       \fbox{\includegraphics[height=3in]{smallwireelement.png}}
       \caption{Forces on the wire element shown alongside the soap film}
   \end{subfigure}
   \begin{subfigure}[t]{0.5\textwidth}
       \centering
       \fbox{\includegraphics[height=2.4in]{forcebalance.png}}
       \caption{Forces on the wire element. Only tension in the wire opposes the deformation caused by surface tension.}
       \label{fig:forcesonwirelement}
   \end{subfigure}
   \caption{Forces on infinitesimal wire element}
\end{figure}
To measure surface tension, we need to establish a relation between surface tension and an observable quantity. The bounding wires holding the soap film bend inwards when the soap solution is flowing between them. The surface tension force which is the only force in horizontal direction pulls the wires inwards. To get the relation between deflection of the bounding wires and surface tension we proceed as follows:
Consider a small element of length $\Delta L$ of the bounding wire as shown in the figure \ref{fig:forcesonwirelement}. As the wire has a weak curvature we can assume that $\Delta L \approx \Delta x$ and x-direction points along gravity for any point on the wire. The local normal and tangent to the element form the x and y axis respectively. Let the tension at point A be $T$ and at point B be $ T + (\partial{T}/\partial{x}) \Delta{x} $\\
\noindent
\underline{Balancing the forces in the x-direction (vertical direction):}
\begin{equation}
T - (T +  \frac{\partial{T}}{\partial{x}} \Delta{x}) + \rho \pi r^2 g \Delta{x} + mu \frac{\partial{u}}{\partial{y}} \pi r \Delta{x} = 0,
\end{equation}
Cancelling out $T$ and $\Delta{x}$ leaves us with:

\begin{equation}
\underbrace{-\frac{\partial{T}}{\partial{x}}}_{Tension\\force} + \underbrace{\rho \pi r^2 g}_{gravitational\ force} + \underbrace{\mu \frac{\partial{u}}{\partial{y}} \pi r }_{viscous\\force} =0.
\end{equation}

The radius $r$ of the nylon wires is around 100 $\mu m$ i.e. $10^{-4}$ m and density of nylon is around 1150 kg/m$^3$ which gives us the gravitaitonal force per unit length to be 10$^{-4}$ N/m. To find the viscous drag force, $\mu = 8.9 \times 10^{-3} \, {\rm Pa\cdot s}$ and $\frac{\partial u}{\partial y} \approx \frac{150 cm/s}{0.15 cm} = 1000 \, {\rm s^{-1}}$ from \cite{Rutgers1996}. Hence, the viscous drag comes out to be $\approx 10^{-4} $ N/m. The force balance in the vertical direction gives us that $ \partial{T} / \partial{x} \approx 0 $ and hence the tension $T$ is constant throughout the wire equal to the half of the suspended weight attached at the bottom of the soap film.\\


\noindent
\underline{Balancing the forces in the y-direction (horizontal direction):}
\noindent
The diameter of the wire is $\approx$ \SI{100}{\micro\metre} while the thickness of the film around center is $\approx$ \SI{10}{\micro\metre}. This causes the film to attach itself to the wire at a different length than the thickness and this region is called as the plateau border \citep{couder1989}. The change in thickness is accompanied by a curvature which changes the pressure inside the soap film near the wire, shown by $P_B$ in Fig. \ref{fig:y-zplane}. Although there are different pressures around the wire, we can conveniently avoid evaluating $P_B$ by choosing a control volume that includes the point B inside it. Hence, our control volume (shown as dashed rectangle in Fig. \ref{fig:y-zplane}) extends from the wire into the soap film where the curvature effects are negligible. 

\begin{figure}[h!]
\centering
\includegraphics[scale=0.7]{y-zdirectionforcebalance.png}
\caption{Cross section of the film in y-z direction. Control volume shown by dashed rectangle. Pressure at point B, $P_B$, is different than $P_atm$ due to laplace pressure difference but our control volume extends till point A avoiding the need to calculate pressure at point B. Surface tension $\sigma$ is same at A and B because there is no flow along y-direction.}
\label{fig:y-zplane}
\end{figure}

\noindent
As there is no flow in y-direction,  $ \partial{\sigma}/\partial{y} =0$, i.e. surface tension tension does not change along y-direction for all $x$ .  The surface tension is balanced by the horizontal component of tension in the wires and the shear force $V$ inside the wire which arises due to the bending of the wire \citep{popov}. For an infinitesimal wire element the force balance comes out to be  
\begin{equation}
2 \sigma \Delta{x} - 2 T sin(\Delta \theta) - \frac{\partial V}{\partial x} \Delta x = 0  ,
\label{eq:ydirectionforcebalance}
\end{equation}
as $\Delta \theta \rightarrow 0,$ $sin(\Delta \theta) \approx tan(\Delta \theta)$ and from Euler-Bernoulli beam theory \citep{mechanics-of-solids} $ V = {\partial M}/{\partial x} = EI {\partial \kappa}/{\partial x} $, which gives 
\begin{equation}
2 \sigma \Delta{x} - 2 T tan(\Delta \theta) - EI \frac{\partial^2 \kappa}{\partial x^2} \Delta x =0, 
\end{equation}
\\
Lets say the radius of curvature of the wire element is $R$, where $\kappa = 1/R = \partial{^2 y}/\partial{x^2}.$ Then $tan(\Delta \theta) = \Delta{x}/2R$ which gives
\begin{equation}
2 \sigma - T  \frac{\partial{^2 y}}{\partial{x^2}} - EI \frac{\partial^4 y}{\partial x^4} =0. 
\label{eq:sigkapt}
\end{equation}\\
Let the maximum deflection in the wire be given by $\delta$. Hence $ \partial ^2 y / \partial x^2 \sim \delta/L^2 $ and $ \partial ^4 y / \partial x^4 \sim \delta/L^4 $.  Assuming bending rigidity is negligible, i.e.  $ 2 \sigma - T  {\partial{^2 y}}{\partial{x^2}}  = 0$ and solving for y with boundary conditions $y=0$ at $x=0, L$,  we get $ \delta = \sigma L^2 / 4T $ \footnote{Here $L$ is length of wire along vertical direction, and value of $\delta = \sigma L^2 / 4T \approx 2$ cm is consistent with experimental observations. If we assume there is no tension in the wire due to the suspended weight the wire becomes a simply supported beam. By the Euler-Bernoulli beam theory, the maximum deflection as given in \citep{mechanics-of-solids} comes out to be $\delta = 5 \sigma L^4 / 384 E I = 6 \times 10^{3}$ m, which is practically infeasible for our system.}.  Using $\delta = \sigma L^2 / 4T$ the tension term scales as $ T \partial^2y / \partial^2x \sim \sigma $ and  the bending term scales as $ EI \partial^4 y/ \partial x^4 \sim \sigma E I /  T L^2$. With $T=0.49$ N , $L \approx 1$ m, $ I \approx r^4 = 10^{-16}$  m$^4$ , and $ E \approx 10^9$ Pa we get $EI/TL^2 \approx 5 \times 10^{-8}$ N/m and hence the third term of Eq. \ref{eq:sigkapt} is negligible as compared to the first two terms as the first two terms scale as $\sigma$ whose value is around 0.05 N/m. The relation between the surface tension and the curvature of the wire comes out to be 
\begin{equation}
2 \sigma  = T \frac{\partial^2 y}{\partial x^2}. 
\end{equation} 

\newpage
\section{Relation between Elasticity and the Marangoni Wave Speed}
\label{sec:appendixelasticitywavespeed}
\begin{figure}[h!]
\centering
\includegraphics[scale=0.4]{wavespeed.png}
\caption{Control volume used to derive wave speed in soap films. Wave front in the center.}
\label{fig:wavespeed}
\end{figure}
Consider a slab of soap film as shown in Fig. \ref{fig:wavespeed}. Let the wave front travel with speed $V_m$. On the frame of reference of wave front, the elemental slab of soap film moves a speed $V_m$. The passing of wave front introduces changes in speed, thickness and surface tension as $V_m, dh$ and $d\sigma$ respectively.
Continuity equation in the integral form applied to the above volume of length $l$ gives us:
\begin{equation}
V_{m} h l = (V_m + dV_m)(h+dh)l
\end{equation}
which gives us
\begin{equation}
h dV_m = -V_M dh
\label{eq:conti}
\end{equation}
Applying momentum equation and neglecting higher order terms gives us:
\begin{equation}
V_m^2 \rho dh + 2V_m dV_m \rho h = - 2 d\sigma
\end{equation}
and by using \ref{eq:conti}
\begin{equation}
V_m = \sqrt{\frac{2 d\sigma}{\rho dh}}
\label{eq:elast1}
\end{equation}
By using the definition of Elasticity $E = 2  \left( \frac{d\sigma}{dh/h}\right)$ and substituting it in equation \ref{eq:elast1} we get the following relation between elasticity and the wave speed:
\begin{equation}
V_m = \sqrt{\frac{E}{\rho h}}
\label{eq:elastiwave2}
\end{equation}



\newpage

\bibliography{thesisproject}
%\bibliographystyle{plain}
\end{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\noindent
\rule{\textwidth}{10pt}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



